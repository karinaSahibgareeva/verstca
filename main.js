$(document).ready(function () {
// block1
    $(".services-slider").slick({
        rows: 1,
        slidesPerRow: 4,
        swipe: false,
        infinite: false,
        arrows:false,
        responsive: [
            {
                breakpoint: 1285,
                settings: {
                    rows: 2,
                    slidesPerRow: 2,
                    swipe: true,
                    dots: true,
                }
            },
            {
                breakpoint: 545,
                settings: {
                    rows: 1,
                    slidesPerRow: 1,
                    infinite: true,
                    swipe: true,
                    dots: true,
                }
            }
        ]
    });
// block2
$(".work-examples-card-slider").slick({
    rows: 2,
    slidesPerRow: 2,
    swipe: false,
    infinite: true,
    arrows:false,
    responsive: [
        {
            breakpoint: 1025,
            settings: {
                rows: 1,
                slidesPerRow: 1,
                swipe: true,
                dots:false
            }
        },
        {
            breakpoint: 545,
            settings: {
                rows: 1,
                slidesPerRow: 1,
                swipe: true,
                dots: true,
            }
        }
    ]
});
$(".work-examples-img-slider").slick({
    slidesToShow: 1,
    swipe: true,
    infinite: true,
    arrows:true,
    dots:true,
    prevArrow:"<img class='prev-btn' src='img/left.png'>",
    nextArrow:"<img class='next-btn' src='img/right.png'>",
    responsive: [
        {
            breakpoint: 1025,
            settings: {
                rows: 1,
                slidesPerRow: 1,
                swipe: false,
            }
        },
        {
            breakpoint: 545,
            settings: {
                rows: 1,
                slidesPerRow: 1,
                swipe: false
            }
        }
    ]
});
// block5
$(".about-cards-slider").slick({
    rows: 1,
    slidesPerRow: 3,
    // slidesToShow: 3,
    swipe: false,
    infinite: false,
    arrows:false,
    responsive: [
        {
            breakpoint: 1025,
            settings: {
                rows: 2,
                slidesPerRow: 2,
                dots:false
            }
        },
        {
            breakpoint: 825,
            settings: {
                rows: 2,
                slidesPerRow: 2,
                dots:false
            }
        },
        {
            breakpoint: 545,
            settings: {
                rows: 1,
                slidesPerRow: 1,
                infinite: true,
                swipe: true,
                dots: true
            }
        }
    ]
});
// block6
$(".customer-reviews-slider").slick({
    slidesToShow: 3.5,
    swipe: true,
    // centerMode:true,
    // initialSlide:0,
    infinite: false,
    arrows:true,
    dots:false,
    arrows:true,
    prevArrow:"<img class='rewievs-prev-btn' src='img/grey-left.png'>",
    nextArrow:"<img class='rewievs-next-btn' src='img/blue-right.png'>",
    responsive: [
        {
            breakpoint: 1025,
            settings: {
                slidesToShow: 1.2,
                dots:true,
                arrows:false,
            }
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                infinite: true,
                dots:true,
                arrows:false,
            }
        },
        {
            breakpoint: 545,
            settings: {
                slidesToShow: 1,
                infinite: true,
                swipe: true,
                dots: true,
                arrows:false,
            }
        }
    ]
});


});